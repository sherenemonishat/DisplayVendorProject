package com.framework.acme;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DisplayVendorPage extends ProjectMethods{

	public DisplayVendorPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//table[@class='table']") WebElement table;
	public void displayVendor() {
			
		
		List<WebElement> value = table.findElements(By.tagName("td"));
		
			System.out.println("The Vendor name is : " + value.get(0).getText());
	}
}
