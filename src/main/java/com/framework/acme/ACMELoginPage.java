package com.framework.acme;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ACMELoginPage extends ProjectMethods {

	public ACMELoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="email") WebElement loginId;
	@FindBy(how = How.ID, using="password") WebElement pwd;
	@FindBy(how = How.ID, using="buttonLogin") WebElement login;
	public ACMELoginPage enterLoginId(String data) {
		clearAndType(loginId, data);
		return this;
	}
	public ACMELoginPage enterPassword(String data) {
		clearAndType(pwd, data);
		return this;
	}
	
	public void clickLogin() {
		click(login);
	}
	
	
}
