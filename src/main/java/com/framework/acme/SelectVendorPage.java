package com.framework.acme;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class SelectVendorPage extends ProjectMethods{

	public SelectVendorPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH,using="//button[@type='button']/i[@class='fa fa-truck']") WebElement vendor;
	@FindBy(how = How.LINK_TEXT, using = "Search for Vendor") WebElement searchVendor;
	public void selectVendor() {
		Actions action = new Actions(driver);
	    action.moveToElement(vendor).build().perform();
	    click(searchVendor);
		
	}
}
