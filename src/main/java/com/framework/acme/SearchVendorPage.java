package com.framework.acme;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class SearchVendorPage extends ProjectMethods{

	public SearchVendorPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "vendorTaxID") WebElement vendorID;
	@FindBy(how = How.ID, using = "buttonSearch") WebElement search;
	public SearchVendorPage enterVendorID(String Id) {
		clearAndType(vendorID, Id);
		return this;
	}
	
	public void clickSearch() {
		click(search);
	}
	
}
