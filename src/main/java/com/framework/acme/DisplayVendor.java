package com.framework.acme;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DisplayVendor {

	public static void main(String args[]) {
		
	
	
	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	ChromeDriver driver= new ChromeDriver();
	driver.get("https://acme-test.uipath.com/account/login");
	driver.manage().window().maximize();
	
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	driver.findElementById("email").sendKeys("sherenemonishat@gmail.com");
	driver.findElementById("password").sendKeys("testleaf");
	driver.findElementById("buttonLogin").click();
	
	WebElement element = driver.findElementByXPath("//button[@type='button']/i[@class='fa fa-truck']");
    Actions action = new Actions(driver);

    action.moveToElement(element).build().perform();
    
    driver.findElementByLinkText("Search for Vendor").click();
    
    driver.findElementById("vendorTaxID").sendKeys("DE767565");
    driver.findElementById("buttonSearch").click();
    
    WebElement table = driver.findElementByXPath("//table[@class='table']");
	
	//Rows idendification
	List<WebElement> value = table.findElements(By.tagName("td"));
	

		System.out.println("The Vendor name is : " + value.get(0).getText());
	
	}
}
