package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.acme.ACMELoginPage;
import com.framework.acme.DisplayVendorPage;
import com.framework.acme.SearchVendorPage;
import com.framework.acme.SelectVendorPage;
import com.framework.design.ProjectMethods;

public class DisplayVendorPOM extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001";
		author = "Sherene";
		category = "Smoke";
		testNodes = "test";
	}
	
	@Test
	public void vendorDisplay() {
		
		new ACMELoginPage().enterLoginId("sherenemonishat@gmail.com")
		                   .enterPassword("testleaf")
		                   .clickLogin();
		
		new SelectVendorPage().selectVendor();
		
		new SearchVendorPage().enterVendorID("DE767565")
		                      .clickSearch();
		
		new DisplayVendorPage().displayVendor();
		
		
    }
	
}
